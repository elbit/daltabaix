		
	<?php do_action('anderson_before_footer'); ?>
	
	<div id="footer-wrap">
		
		<footer id="footer" class="container clearfix" role="contentinfo">
			
			<nav id="footernav" class="clearfix" role="navigation">
				<?php 
					// Get Navigation out of Theme Options
					wp_nav_menu(array('theme_location' => 'footer', 'container' => false, 'menu_id' => 'footernav-menu', 'echo' => true, 'fallback_cb' => '', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 1));
				?>
			</nav>
			
			<div class="footer-content">

				

				<div class="footer-content-colaboradors">

				<a href=""> <img src="<?php echo get_template_directory_uri(); ?>/images/logo-amics.png"> </a>
				 <a href=""> <img src="<?php echo get_template_directory_uri(); ?>/images/logo-traska.png"> </a>

				 Amb el suport de:

				 <a href="https://www.molinsderei.cat"> <img src="https://daltabaix.cat/wp-content/uploads/2015/04/Logotip-color-central-negatiu-baixa.png"  alt="ajuntament molins de rei"> </a> 
				 

				 </div>

				 <p class="footer-content-info">
					Daltabaix @  <?php echo date('Y'); ?> | <a href="mailto:info@daltabaix.cat">info@daltabaix.cat</a> 	</p>
				

			</div>
			
		</footer>

	</div>
	
</div><!-- end #wrapper -->

<?php wp_footer(); ?>
</body>
</html>