Anderson Lite WordPress Theme, Copyright 2014 ThemeZee.com
Anderson Lite is distributed under the terms of the GNU GPL

===================================
Thanks for Downloading Anderson Lite
===================================

Thank you for downloading my theme. 
If you have any questions that are beyond the scope of this help file, 
please visit themezee.com. Thanks so much!

It is completely optional, but if you like the Theme we would appreciate it if 
you keep the credit link at the bottom.

-----------------------------------
Table of Contents

    * A) Requirements
    * B) Installation
    * C) Theme Options
    * D) Theme Documentation
    * E) Page Templates
    * F) Theme License
	* G) Licenses of bundled resources
-----------------------------------

===================================
A) Requirements
===================================

The theme has been tested on all major browsers including Firefox, Opera, Chrome,  
Internet Explorer and Safari. Your browser should have JavaScript enabled to make this theme work!

The theme works on the latest Wordpress version without any problems. Previous versions have not been tested
and are not supported. I suggest to update your wordpress installation, especially due to security flaws.

===================================
B) Installation
===================================

   1. Automatic Installation
      Go to WP-Admin > Appearance > Themes > Install Themes > Upload and choose the theme zip folder.

   2. Manual Installation
      Upload the theme files to your wordpress theme folder wp-content/themes and activate the theme in
      your wordpress admin panel. That's it!

To find out more about installing WordPress themes please also see http://themezee.com/docs/install-wordpress-themes/

===================================
C) Theme Options
===================================

Anderson Lite supports the awesome Theme Customizer for all theme settings. 
Go to WP-Admin > Appearance > Customize to open the Customizer.

===================================
D) Theme Documentation
===================================

Need any help to setup and configure this theme? We got you covered with an extensive theme documentation on our website.
http://themezee.com/docs/anderson-documentation/

===================================
E) Page Templates
===================================

We have included a few, nice page templates for advanced content presentation.

	1. Fullwidth Template 
	This template shows a static page without the blog sidebar.
	
	2. Magazine Frontpage Template
	coming soon with the next updates

===================================
F) Theme License
===================================

Anderson Lite is released under the GNU general public license. 
That means you can use this theme on all of your websites - for personal or commercial purposes!

===================================
G) Licenses of bundled resources
===================================

	1. Genericons, Copyright 2014 Automattic, Inc.
	The theme uses the Genericons icon font for postmeta, menu and social icons licensed under GNU GPL.
	Source URL: http://genericons.com
	License of Genericon Icon font: http://www.gnu.org/licenses/gpl.html

	2. Bundled Images, Copyright 2014 ThemeZee.com
	Images such as the default slider image and ad block image included in this theme package are created by myself and licensed under GNU GPL.
	License URL of Logo / Background images: http://www.gnu.org/licenses/gpl.html

	3. Default Fonts (Share, Carme)
	Default Fonts used in the theme are from the Google Font API and are GPL-compatible licensed (SIL Open Font License (OFL))
	https://www.google.com/fonts/specimen/Carme
	http://www.google.com/fonts/specimen/Share
	
	4. Screenshot Photography (used on screenshot.png)
	We only use verified images from Pixabay for the screenshot.png, which are licensed under CC0 (GPL compatible).
	Source URL: http://pixabay.com
	Image 1: http://pixabay.com/en/jet-engine-turbine-jet-airplane-371412/
	Image 2: http://pixabay.com/en/by-wlodek-old-close-hack-closed-428549/
	Image 3: http://pixabay.com/en/typewriter-letters-keyboard-keys-472850/
	Image 4: http://pixabay.com/en/toothed-belt-drive-details-209677/
	Image 5: http://pixabay.com/en/rusty-metal-old-machine-185531/
	Image 6: http://pixabay.com/en/microscope-research-lab-laboratory-385364/
	Image 7: http://pixabay.com/en/glass-facade-colorful-architecture-200888/
	Image 8: http://pixabay.com/en/board-electronics-computer-453758/
	Image 9: http://pixabay.com/en/microscope-research-lab-laboratory-385364/
	License URL: http://creativecommons.org/publicdomain/zero/1.0/deed.de

===================================
Enjoy!
===================================

Once again, thank you so much for downloading this theme. 